package utils_test

import (
	"testing"

	"gitlab.com/ownageoss/utils"
)

func TestIsDigitsOnly(tester *testing.T) {
	tester.Parallel()

	testcases := []struct {
		name  string
		input string
		want  bool
	}{
		{
			name:  "digits only",
			input: "0123456789",
			want:  true,
		},
		{
			name:  "alphanumeric",
			input: "a01bc",
			want:  false,
		},
		{
			name:  "symbols",
			input: ">|*&",
			want:  false,
		},
		{
			name:  "nothing",
			input: "",
			want:  false,
		},
	}
	for _, tc := range testcases {
		tester.Run(tc.input, func(tester *testing.T) {
			tester.Parallel()
			result := utils.IsDigitsOnly(tc.input)
			if result != tc.want {
				tester.Errorf("Input: %v, Want: %v, Got: %v", tc.input, tc.want, result)
			}
		})
	}
}
