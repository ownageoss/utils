package utils

import (
	"errors"
	"unicode/utf8"

	"golang.org/x/crypto/bcrypt"
)

// HashString - function to hash a string.
func HashString(sesame string) (string, error) {
	if !utf8.ValidString(sesame) {
		return "", errors.New("input is not valid UTF-8")
	}
	bytes, err := bcrypt.GenerateFromPassword([]byte(sesame), bcrypt.DefaultCost)
	if err != nil {
		return "", err
	}
	return string(bytes), nil
}

// CheckStringWithHash - function to check if a string and hash(string) match.
func CheckStringWithHash(sesame, hash string) bool {
	if !utf8.ValidString(sesame) || !utf8.ValidString(hash) {
		return false
	}
	err := bcrypt.CompareHashAndPassword([]byte(hash), []byte(sesame))
	// If err == nil, then there was a match
	return err == nil
}
