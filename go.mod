module gitlab.com/ownageoss/utils

go 1.23.0

require (
	github.com/juju/ratelimit v1.0.2
	golang.org/x/crypto v0.36.0
)

require (
	github.com/kr/pretty v0.3.1 // indirect
	gopkg.in/check.v1 v1.0.0-20201130134442-10cb98267c6c // indirect
)
