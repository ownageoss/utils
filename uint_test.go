package utils_test

import (
	"testing"

	"gitlab.com/ownageoss/utils"
)

func FuzzIntToUint32(f *testing.F) {
	testcases := []int{
		-9223372036854775808,
		-9223372036,
		-2147483648,
		-21474,
		-1,
		0,
		1,
		21474,
		2147483648,
		9223372036,
		9223372036854775807,
	}
	for _, tc := range testcases {
		f.Add(tc) // Use f.Add to provide a seed corpus
	}
	f.Fuzz(func(t *testing.T, signedNumber int) {
		// #nosec G115 - this is a test case
		if signedNumber >= 0 && utils.IntToUint32(signedNumber) != uint32(signedNumber) {
			t.Errorf("signedNumber: %v, unsignedNumber: %v", signedNumber, uint32(signedNumber))
		}

		// #nosec G115 - this is a test case
		if signedNumber < 0 && utils.IntToUint32(signedNumber) != 0 {
			t.Errorf("signedNumber: %v, unsignedNumber: %v", signedNumber, uint32(signedNumber))
		}
	})
}

func FuzzIntToUint64(f *testing.F) {
	testcases := []int{
		-9223372036854775808,
		-9223372036,
		-2147483648,
		-21474,
		-1,
		0,
		1,
		21474,
		2147483648,
		9223372036,
		9223372036854775807,
	}
	for _, tc := range testcases {
		f.Add(tc) // Use f.Add to provide a seed corpus
	}
	f.Fuzz(func(t *testing.T, signedNumber int) {
		// #nosec G115 - this is a test case
		if signedNumber >= 0 && utils.IntToUint64(signedNumber) != uint64(signedNumber) {
			t.Errorf("signedNumber: %v, unsignedNumber: %v", signedNumber, uint64(signedNumber))
		}

		// #nosec G115 - this is a test case
		if signedNumber < 0 && utils.IntToUint64(signedNumber) != 0 {
			t.Errorf("signedNumber: %v, unsignedNumber: %v", signedNumber, uint64(signedNumber))
		}
	})
}
