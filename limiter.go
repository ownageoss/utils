package utils

import (
	"time"

	jujuRateLimit "github.com/juju/ratelimit"
)

// TokenBucketLimiter - rate limiting definition.
type TokenBucketLimiter struct {
	limiter         *jujuRateLimit.Bucket
	maxWaitDuration time.Duration
}

// TokenBucketRateLimiter creates a tokenBucketLimiter.
func TokenBucketRateLimiter(
	fillInterval time.Duration,
	capacity int64,
	quantum int64,
	maxWaitDuration time.Duration,
) *TokenBucketLimiter {
	return &TokenBucketLimiter{
		limiter:         jujuRateLimit.NewBucketWithQuantum(fillInterval, capacity, quantum),
		maxWaitDuration: maxWaitDuration,
	}
}

// Limit takes 1 token from the bucket only if it needs to wait for no greater than maxWaitDuration.
func (bucket *TokenBucketLimiter) Limit() bool {
	return !bucket.limiter.WaitMaxDuration(1, bucket.maxWaitDuration)
}
