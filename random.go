package utils

import (
	"crypto/rand"
	"errors"
	"math/big"
	"strconv"
)

// RandomInt - make a random integer of a certain length (>=1).
func RandomInt(length int) (int, error) {
	intString, err := RandomString(length, []rune("0123456789"))
	if err != nil {
		return 0, err
	}
	if intString[0:1] == "0" {
		leadDigit, _ := RandomString(1, []rune("123456789"))
		intString = leadDigit + intString[1:]
	}
	return strconv.Atoi(intString)
}

// RandomDigits - make a random string of digits of a certain length.
func RandomDigits(length int) (string, error) {
	return RandomString(length, []rune("0123456789"))
}

// RandomLetters - make a random string of letters of a certain length.
func RandomLetters(length int) (string, error) {
	return RandomString(length, []rune("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"))
}

// RandomAlphaNumeric - make a random alphanumeric string of certain length.
func RandomAlphaNumeric(length int) (string, error) {
	return RandomString(
		length,
		[]rune("0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"),
	)
}

// RandomString - make a random string of certain length with some runes.
func RandomString(stringLength int, runes []rune) (string, error) {
	if stringLength < 1 {
		return "", errors.New("random length must be >= 1")
	}
	if len(runes) == 0 {
		runes = []rune("0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz")
	}
	bytes := make([]rune, stringLength)
	for i := range bytes {
		randomNumber, err := rand.Int(rand.Reader, big.NewInt(int64(len(runes))))
		if err != nil {
			return "", err
		}
		bytes[i] = runes[randomNumber.Int64()]
	}
	return string(bytes), nil
}
