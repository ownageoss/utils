package utils

import (
	"encoding/json"
)

// ErrorDetail - struct to store error message.
type ErrorDetail struct {
	Error string `json:"error,omitempty"`
}

// JSONErrorMessage - construct JSON error message.
func JSONErrorMessage(message string) []byte {
	errorDetail := ErrorDetail{
		Error: message,
	}
	errorJSON, err := json.Marshal(errorDetail)
	if err != nil {
		return nil
	}
	return errorJSON
}
