package utils

import (
	"net/url"
	"unicode/utf8"
)

// IsValidUrl - tests a string to determine if it is a well-structured url or not.
// https://golangcode.com/how-to-check-if-a-string-is-a-url/
func IsValidURL(input string) bool {
	if !utf8.ValidString(input) {
		return false
	}
	_, err := url.ParseRequestURI(input)
	if err != nil {
		return false
	}

	result, err := url.Parse(input)
	if err != nil || result.Scheme == "" || result.Host == "" {
		return false
	}

	return true
}
