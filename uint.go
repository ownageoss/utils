package utils

// Convert any signed integer to uint32.
// Values < 0 are returned as 0.
func IntToUint32[V int | int8 | int16 | int32 | int64](x V) uint32 {
	if x < 0 {
		return 0
	}

	return uint32(x)
}

// Convert any signed integer to uint64.
// Values < 0 are returned as 0.
func IntToUint64[V int | int8 | int16 | int32 | int64](x V) uint64 {
	if x < 0 {
		return 0
	}

	return uint64(x)
}
