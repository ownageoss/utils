package utils_test

import (
	"testing"

	"gitlab.com/ownageoss/utils"
)

type astruct struct {
	Field1 string
	Field2 int
}

type bstruct struct {
	Field3 int
	Field4 string
}

func TestMarshalAndMergeJSON(tester *testing.T) {
	tester.Parallel()
	a := astruct{"x", 1}
	b := bstruct{2, "y"}

	tester.Log(a, b)

	json, err := utils.MarshalAndMergeJSON(a, b)
	if err != nil {
		tester.Fatal(err)
	}

	tester.Log(string(json))
}

func TestLoadJSON(tester *testing.T) {
	tester.Parallel()
	file := "test.json"
	destination := make(map[string]string)
	if err := utils.LoadJSON(file, &destination); err != nil {
		tester.Fatal(err)
	}
}
