package utils_test

import (
	"testing"
	"unicode/utf8"

	"gitlab.com/ownageoss/utils"
)

func FuzzHashString(f *testing.F) {
	testcases := []string{"abc,123", " ", "!12345@%9_"}
	for _, tc := range testcases {
		f.Add(tc) // Use f.Add to provide a seed corpus
	}
	f.Fuzz(func(t *testing.T, code string) {
		hashCode, err := utils.HashString(code)
		if err != nil {
			return
		}
		if code == hashCode {
			t.Errorf("Code: %q, Hashcode: %q", code, hashCode)
		}
		if utf8.ValidString(code) && !utf8.ValidString(hashCode) {
			t.Errorf("Hashed code produced invalid UTF-8 string %q", hashCode)
		}
	})
}

func FuzzCheckStringWithHash(f *testing.F) {
	testcases := []string{"abc,123", " ", "!12345@%9_"}
	for _, tc := range testcases {
		f.Add(tc) // Use f.Add to provide a seed corpus
	}
	f.Fuzz(func(t *testing.T, code string) {
		hashCode, err := utils.HashString(code)
		if err != nil {
			return
		}
		match := utils.CheckStringWithHash(code, hashCode)
		if !match {
			t.Errorf("Code: %q, Hashcode: %q", code, hashCode)
		}
	})
}
