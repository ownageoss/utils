# Utils

This package contains some general purpose utility functions for Go.

## Example usage

```go
// main.go
package main

import (
	"fmt"

	"gitlab.com/ownageoss/utils"
)

func main() {
	fmt.Println(utils.HashString("secret"))
}

```

Refer to `_test.go` files for more examples.

## Versioning

This project uses [Semantic Versioning 2.0.0](https://semver.org/).

## Project status

This library is used in production environments and is actively maintained.

## Supported Go versions

Go version support is aligned with the Go's [release policy](https://go.dev/doc/devel/release#policy).

## Software Bill of Materials (SBOM)

```
               _      
 ___ _ __   __| |_  __
/ __| '_ \ / _` \ \/ /
\__ \ |_) | (_| |>  < 
|___/ .__/ \__,_/_/\_\
    |_|               

 📂 SPDX Document gitlab.com/ownageoss/utils
  │ 
  │ 📦 DESCRIBES 1 Packages
  │ 
  ├ utils
  │  │ 🔗 30 Relationships
  │  ├ CONTAINS FILE .gitlab-ci.yml (.gitlab-ci.yml)
  │  ├ CONTAINS FILE Makefile (Makefile)
  │  ├ CONTAINS FILE .gitignore (.gitignore)
  │  ├ CONTAINS FILE digit_test.go (digit_test.go)
  │  ├ CONTAINS FILE digit.go (digit.go)
  │  ├ CONTAINS FILE README.md (README.md)
  │  ├ CONTAINS FILE LICENSE (LICENSE)
  │  ├ CONTAINS FILE error.go (error.go)
  │  ├ CONTAINS FILE error_test.go (error_test.go)
  │  ├ CONTAINS FILE go.sum (go.sum)
  │  ├ CONTAINS FILE go.mod (go.mod)
  │  ├ CONTAINS FILE hash.go (hash.go)
  │  ├ CONTAINS FILE json_test.go (json_test.go)
  │  ├ CONTAINS FILE hash_test.go (hash_test.go)
  │  ├ CONTAINS FILE json.go (json.go)
  │  ├ CONTAINS FILE limiter_test.go (limiter_test.go)
  │  ├ CONTAINS FILE limiter.go (limiter.go)
  │  ├ CONTAINS FILE random_test.go (random_test.go)
  │  ├ CONTAINS FILE random.go (random.go)
  │  ├ CONTAINS FILE sqlnull.go (sqlnull.go)
  │  ├ CONTAINS FILE test.json (test.json)
  │  ├ CONTAINS FILE uint.go (uint.go)
  │  ├ CONTAINS FILE sqlnull_test.go (sqlnull_test.go)
  │  ├ CONTAINS FILE uint_test.go (uint_test.go)
  │  ├ CONTAINS FILE url.go (url.go)
  │  ├ CONTAINS FILE url_test.go (url_test.go)
  │  ├ CONTAINS FILE utils.png (utils.png)
  │  ├ CONTAINS FILE utils.spdx (utils.spdx)
  │  ├ DEPENDS_ON PACKAGE github.com/juju/ratelimit@v1.0.2
  │  └ DEPENDS_ON PACKAGE golang.org/x/crypto@v0.36.0
  │ 
  └ 📄 DESCRIBES 0 Files
```