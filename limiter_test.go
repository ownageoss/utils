package utils_test

import (
	"testing"
	"time"

	"gitlab.com/ownageoss/utils"
)

func TestTokenBucketRateLimiter(tester *testing.T) {
	tester.Parallel()
	limiter := utils.TokenBucketRateLimiter(time.Second, 5, 2, 5*time.Second)
	// limiter.Limit() returns TRUE if there are no tokens, i.e. rate limit if there are NO tokens.
	if limiter.Limit() {
		tester.Fatal("Expected at least 1 token in bucket.")
	}
}
