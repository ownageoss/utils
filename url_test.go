package utils_test

import (
	"testing"

	"gitlab.com/ownageoss/utils"
)

func TestIsValidUrl(tester *testing.T) {
	tester.Parallel()

	testcases := []struct {
		url  string
		want bool
	}{
		{
			url:  "ownage.co.nz",
			want: false,
		},
		{
			url:  "",
			want: false,
		},
		{
			url:  "https://ownage.co.nz",
			want: true,
		},
		{
			url:  "https://!12345@%9_",
			want: false,
		},
	}
	for _, tc := range testcases {
		tester.Run(tc.url, func(tester *testing.T) {
			tester.Parallel()
			valid := utils.IsValidURL(tc.url)
			if valid != tc.want {
				tester.Errorf("Input: %v, Want: %v, Got: %v", tc.url, tc.want, valid)
			}
		})
	}
}
