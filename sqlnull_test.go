package utils_test

import (
	"testing"
	"time"

	"gitlab.com/ownageoss/utils"
)

func TestSQLNullString(tester *testing.T) {
	tester.Parallel()
	// Empty string test
	emptyString := utils.SQLNullString("")
	if emptyString.String != "" {
		tester.Fatal("Expected empty string")
	}

	helloString := utils.SQLNullString("hello")
	if helloString.String != "hello" {
		tester.Fatal("Expected hello")
	}
}

func TestSQLNullTime(tester *testing.T) {
	tester.Parallel()
	// Empty string test
	emptyTime := utils.SQLNullTime(time.Time{})
	if emptyTime.Valid {
		tester.Fatal("Expected empty time")
	}

	currentTime := utils.SQLNullTime(time.Now())
	if !currentTime.Valid {
		tester.Fatal("Expected time to be valid")
	}
}
