package utils

import (
	"encoding/json"
	"os"
	"path/filepath"
)

// MarshalAndMergeJSON - marshal arbitrary number of Go values into one JSON structure.
// Be aware that this won't preserve the order of fields.
func MarshalAndMergeJSON(values ...interface{}) ([]byte, error) {
	var merged map[string]interface{}
	for _, value := range values {
		jsonData, err := json.Marshal(value)
		if err != nil {
			return nil, err
		}
		err = json.Unmarshal(jsonData, &merged)
		if err != nil {
			return nil, err
		}
	}
	jsonData, err := json.Marshal(merged)
	if err != nil {
		return nil, err
	}
	return jsonData, nil
}

// LoadJSON - unmarshal a JSON config file into destination object.
func LoadJSON(jsonFileName string, destination interface{}) error {
	// unmarshal a JSON config file into this service object
	file, err := os.Open(filepath.Clean(jsonFileName))
	if err != nil {
		return err
	}
	defer func() {
		if err = file.Close(); err != nil {
			return
		}
	}()
	decoder := json.NewDecoder(file)
	err = decoder.Decode(destination)
	if err != nil {
		return err
	}
	return nil
}
