package utils_test

import (
	"math/rand"
	"regexp"
	"strconv"
	"testing"
	"time"

	"gitlab.com/ownageoss/utils"
)

func TestRandomInt(tester *testing.T) {
	tester.Parallel()

	// Positive test
	length := 10
	random, err := utils.RandomInt(length)
	if err != nil {
		tester.Fatal(err)
	}
	tester.Log("Random integer:", random)
	intString := strconv.Itoa(random)
	if len(intString) != length {
		tester.Fatal("Random integer was not generated correctly.")
	}
	if regexp.MustCompile(`^0`).MatchString(intString) {
		tester.Fatal("Random integer was not generated correctly.")
	}

	// Negative test
	length = 0
	_, err = utils.RandomInt(length)
	if err != nil {
		tester.Log(err)
	} else {
		tester.Fatal()
	}
}

func TestRandomDigits(tester *testing.T) {
	tester.Parallel()

	length := 9

	random, err := utils.RandomDigits(length)
	if err != nil {
		tester.Fatal(err)
	}
	if len(random) != length {
		tester.Fatal("Random numbers were not generated correctly.")
	}
	if len(random) > 0 && !regexp.MustCompile(`^[0-9]+$`).MatchString(random) {
		tester.Fatal("Random numbers were not generated correctly.")
	}

	tester.Log("Random numbers:", random)
}

func TestRandomLetters(tester *testing.T) {
	tester.Parallel()

	length := 10

	random, err := utils.RandomLetters(length)
	if err != nil {
		tester.Fatal(err)
	}
	if len(random) != length {
		tester.Fatal("Random letters were not generated correctly.")
	}
	if len(random) > 0 && !regexp.MustCompile(`^[a-zA-Z]+$`).MatchString(random) {
		tester.Fatal("Random letters were not generated correctly.")
	}

	tester.Log("Random letters:", random)
}

func TestRandomAlphaNumeric(tester *testing.T) {
	tester.Parallel()

	length := 14

	random, err := utils.RandomAlphaNumeric(length)
	if err != nil {
		tester.Fatal(err)
	}
	if len(random) != length {
		tester.Fatal("Random alphanumerics were not generated correctly.")
	}
	if len(random) > 0 && !regexp.MustCompile(`^[a-zA-Z0-9]+$`).MatchString(random) {
		tester.Fatal("Random alphanumerics were not generated correctly.")
	}

	tester.Log("Random alphanumeric:", random)
}

func TestRandomString(tester *testing.T) {
	tester.Parallel()

	// Positive test
	length := 6
	random, err := utils.RandomString(length, []rune("0123456789ABCDEF"))
	if err != nil {
		tester.Fatal(err)
	}
	if len(random) != length {
		tester.Fatal("Random string was not generated correctly.")
	}
	tester.Log("Random string:", random)

	// Negative test
	random, err = utils.RandomString(length, []rune(""))
	if err != nil {
		tester.Fatal(err)
	}
	if len(random) != length {
		tester.Fatal("Random string was not generated correctly.")
	}
	tester.Log("Random string:", random)
}

func TestRandSeed(tester *testing.T) {
	tester.Parallel()

	// length of random strings
	length := 3

	// random numbers with program's initialized seed
	s1, err := utils.RandomDigits(length)
	if err != nil {
		tester.Fatal(err)
	}

	// random numbers with a math rand seed.
	//#nosec G404 -- Math rand is only used for comparison during test run.
	rand.New(rand.NewSource(time.Now().UnixNano()))
	s2, err := utils.RandomDigits(length)
	if err != nil {
		tester.Fatal(err)
	}

	if s1 == s2 {
		tester.Fatal("Duplicate random number, seed is not different enough")
	}

	tester.Log(s1, s2)
}

func TestRandSequence(tester *testing.T) {
	tester.Parallel()

	// Length of random strings.
	// Use a large length to guarantee randomness.
	length := 10

	// Number of random strings to generate.
	// Count should proportionate with length to guarantee randomness.
	// Example: a length of 1 and count of 10 cannot guarantee randomness in sequence.
	count := 100

	s := make([]string, count)
	for i := range count {
		s[i], _ = utils.RandomDigits(length)
	}

	duplicateCount := 0
	for i, v1 := range s {
		for j, v2 := range s {
			if i != j && v1 == v2 {
				duplicateCount++
				tester.Fatal("Duplicate found:", v1)
			}
		}
	}

	tester.Log(duplicateCount, "duplicates found")
	tester.Log(s)
}
