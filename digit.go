package utils

// IsDigitsOnly - checks if a string contains digits only.
func IsDigitsOnly(input string) bool {
	if len(input) == 0 {
		return false
	}

	for _, character := range input {
		if character < '0' || character > '9' {
			return false
		}
	}
	return true
}
