PACKAGE_NAME=utils

updatedeps:
	go get -u ./...
	go mod tidy

sbom:
	bom generate --name gitlab.com/ownageoss/$(PACKAGE_NAME) --output=$(PACKAGE_NAME).spdx .
	bom document outline $(PACKAGE_NAME).spdx

checks:
	gitleaks detect -v --no-git
	golangci-lint run --no-config
	govulncheck ./...
	gosec -tests ./...
	go test -race -v ./...
	go test -race -v -run FuzzHashString -fuzz=FuzzHashString -fuzztime 10s
	go test -race -v -run FuzzCheckStringWithHash -fuzz=FuzzCheckStringWithHash -fuzztime 10s
	go test -race -v -run FuzzIntToUint32 -fuzz=FuzzIntToUint32 -fuzztime 10s
	go test -race -v -run FuzzIntToUint64 -fuzz=FuzzIntToUint64 -fuzztime 10s

brutal:
	golangci-lint run --enable-all