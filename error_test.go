package utils_test

import (
	"testing"

	"gitlab.com/ownageoss/utils"
)

func TestJSONErrorMessage(tester *testing.T) {
	tester.Parallel()
	jsonErrorMessage := utils.JSONErrorMessage("oops, an error occured")
	if jsonErrorMessage == nil ||
		string(jsonErrorMessage) != "{\"error\":\"oops, an error occured\"}" {
		tester.Fatal("Expected : {\"error\":\"oops, an error occured\"}")
	}
}
