package utils

import (
	"database/sql"
	"time"
)

// SQLNullString - Insert NULL into DB instead of emtpy string.
func SQLNullString(input string) sql.NullString {
	if len(input) == 0 {
		return sql.NullString{}
	}
	return sql.NullString{
		String: input,
		Valid:  true,
	}
}

// SQLNullTime - Insert NULL into DB instead of time.Time{}.
func SQLNullTime(input time.Time) sql.NullTime {
	if input.IsZero() {
		return sql.NullTime{}
	}
	return sql.NullTime{
		Time:  input,
		Valid: true,
	}
}
